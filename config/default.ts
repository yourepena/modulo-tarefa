export default {
    HOST: 'http://localhost',
    PORT: 3002,
    MONGO_URI: 'mongodb://localhost:27017/sins',
};