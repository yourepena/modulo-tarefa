import { ModelType, pre, prop, Typegoose } from 'typegoose';
import { schemaOptions } from 'modulo-comum/src/comum/comum.model';
import { TarefaStatus } from './tarefa-status.enum';

@pre<Tarefa>('findOneAndUpdate', function(next) {
    this._update.updatedAt = new Date(Date.now());
    next();
})
export class Tarefa extends Typegoose {
    @prop({
        required: [true, 'Nome da tarefa é obrigatória'],
        unique: true,
        minlength: [5, 'O nome da Tarefa deve conter mais de 5 caracteres'],
    })
    nome: string;

    @prop() nomeProjeto?: string;

    @prop({ default: Date.now() })
    createdAt?: Date;

    @prop({ default: Date.now() })
    updatedAt?: Date;

    @prop({ enum: TarefaStatus, default: TarefaStatus.EmDia })

    @prop() _id: string;

    static get model(): ModelType<Tarefa> {
        return new Tarefa().getModelForClass(Tarefa, { schemaOptions });
    }

    static get modelName(): string {
        return this.model.modelName;
    }
}

export const TarefaModel = new Tarefa().getModelForClass(Tarefa, { schemaOptions });
