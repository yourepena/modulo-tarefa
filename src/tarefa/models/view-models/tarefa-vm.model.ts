import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { ComumModelVm } from 'modulo-comum/src/comum/comum.model';

export class TarefaVm extends ComumModelVm {
    @ApiModelProperty() nome: string;
    @ApiModelPropertyOptional() nomeProjeto?: string;
}
