export enum TarefaStatus {
    Atrasado = 'Atrasado',
    EmDia = 'Em dia',
    Concluido = 'Concluído',
}
