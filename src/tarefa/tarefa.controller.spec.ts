import { Test, TestingModule } from '@nestjs/testing';
import { TarefaController } from './tarefa.controller';

describe('Tarefa Controller', () => {
  let module: TestingModule;
  
  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [TarefaController],
    }).compile();
  });
  it('should be defined', () => {
    const controller: TarefaController = module.get<TarefaController>(TarefaController);
    expect(controller).toBeDefined();
  });
});
