import { forwardRef, HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { ComumService } from 'modulo-comum/src/comum/comum.service';
import { InjectModel } from '@nestjs/mongoose';
import { MapperService } from './mapper/mapper.service';
import { ModelType } from 'typegoose';
import { Tarefa, TarefaModel } from './models/tarefa.model';
import { TarefaVm } from './models/view-models/tarefa-vm.model';

@Injectable()
export class TarefaService  extends ComumService<Tarefa> {

    constructor(
        @InjectModel(Tarefa.modelName) private readonly _tarefaModel: ModelType<Tarefa>,
        private readonly _mapperService: MapperService,
    ) {
        super();
        this._model = _tarefaModel;
        this._mapper = _mapperService.mapper;
    }

    async register(vm: TarefaVm) {
        const { nome, nomeProjeto } = vm;

        const newTarefa = new TarefaModel();
        newTarefa.nome = nome.trim().toLowerCase();
        newTarefa.nomeProjeto = nomeProjeto;

        try {
            const result = await this.create(newTarefa);
            return result.toJSON() as Tarefa;
        } catch (e) {
            throw new HttpException(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
