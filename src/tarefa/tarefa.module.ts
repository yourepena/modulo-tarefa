import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MapperService } from './mapper/mapper.service';
import { TarefaService } from './tarefa.service';
import { Tarefa } from './models/tarefa.model';
import { TarefaController } from './tarefa.controller';

@Module({
    imports: [MongooseModule.forFeature([{ name: Tarefa.modelName, schema: Tarefa.model.schema }])],
    providers: [MapperService, TarefaService],
    controllers: [TarefaController],
    exports: [TarefaService, MapperService],
})
export class TarefaModule {
}