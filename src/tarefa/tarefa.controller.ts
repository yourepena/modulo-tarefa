import { Body, Controller, HttpException, HttpStatus, Post, Get, Query } from '@nestjs/common';
import {
    ApiBadRequestResponse,
    ApiCreatedResponse,
    ApiImplicitQuery,
    ApiOkResponse,
    ApiOperation,
    ApiUseTags,
} from '@nestjs/swagger';

import { ApiException } from 'modulo-comum/src/comum/api-exception.model';

import { GetOperationId } from 'modulo-comum/src/utilities/get-operation-id.helper';
import { EnumToArray } from 'modulo-comum/src/utilities/enum-to-array.helper';
import { isArray, map } from 'lodash';
import { Tarefa } from './models/tarefa.model';
import { TarefaService } from './tarefa.service';
import { TarefaVm } from './models/view-models/tarefa-vm.model';
import { TarefaStatus } from './models/tarefa-status.enum';
import { MessagePattern } from '@nestjs/microservices';

@Controller('tarefa')
@ApiUseTags(Tarefa.modelName)
export class TarefaController {

    constructor(private readonly _tarefaService: TarefaService) { }

    @Post('register')
    @ApiCreatedResponse({ type: TarefaVm })
    @ApiBadRequestResponse({ type: ApiException })
    @ApiOperation(GetOperationId(Tarefa.modelName, 'Register'))
    async register(@Body() vm: TarefaVm): Promise<TarefaVm> {
        const { nome } = vm;

        if (!nome) {
            throw new HttpException('Nome da tarefa é obrigatório', HttpStatus.BAD_REQUEST);
        }

        let exist;
        try {
            exist = await this._tarefaService.findOne({ nome });
        } catch (e) {
            throw new HttpException(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if (exist) {
            throw new HttpException(`Uma tarefa com o nome ${nome} já existe`, HttpStatus.BAD_REQUEST);
        }

        const newUser = await this._tarefaService.register(vm);
        return this._tarefaService.map<TarefaVm>(newUser);
    }

    @Get()
    @ApiOkResponse({ type: TarefaVm, isArray: true })
    @ApiBadRequestResponse({ type: ApiException })
    @ApiOperation(GetOperationId(Tarefa.modelName, 'GetAll'))
    @ApiImplicitQuery({ name: 'status', enum: EnumToArray(TarefaStatus), required: false, isArray: true })
    async get(@Query('status') status?: TarefaStatus): Promise<TarefaVm[]> {

        const filter = {
            status: null,
        };

        if (status) {
            filter.status = { $in: isArray(status) ? [...status] : [status] };
        }

        try {
            const tarefas = await this._tarefaService.findAll(filter);
            return this._tarefaService.map<TarefaVm[]>(map(tarefas, t => t.toJSON()));
        } catch (e) {
            throw new HttpException(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @MessagePattern({ type: 'get-tarefas' })
    public async getTarefas(): Promise<{}[]> {
        return [
            {
                id: '123',
                name: 'Epic TShirt',
                description: 'A truly epic t-shirt for those who are daring',
                price: 2900,
            },
        ];
    }

}
