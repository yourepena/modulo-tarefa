import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigurationService } from 'modulo-comum/src/configuration/configuration.service';
import { Configuration } from 'modulo-comum/src/configuration/configuration.enum';
import { ComumModule } from 'modulo-comum/src/comum/comum.module';
import { TarefaModule } from './tarefa/tarefa.module';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [ComumModule, MongooseModule.forRoot(ConfigurationService.connectionString, {
      retryDelay: 500,
      retryAttempts: 3,
      useNewUrlParser: true,
  }), TarefaModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  static host: string;
  static port: number | string;
  static isDev: boolean;

  constructor(private readonly _configurationService: ConfigurationService ) {
    AppModule.port = AppModule.normalizePort(_configurationService.get(Configuration.PORT));
    AppModule.host = _configurationService.get(Configuration.HOST);
    AppModule.isDev = _configurationService.isDevelopment;
  }

  private static normalizePort(param: number | string): number | string {
    const portNumber: number = typeof param === 'string' ? parseInt(param, 10) : param;
    if (isNaN(portNumber)) return param;
    else if (portNumber >= 0) return portNumber;
  }

}
